<?php

namespace sisventas;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
	//creacion de tablas
    protected $tabla='categoria';
    //creamos nuestra primary key
    protected $primarykey='idCategoria';

    /*si ponemos esta linea en true se agregaran dos tablas que indicas
    actualizacion y fecha de creacion, como no querremos que se agregen las ponemos en false*/

    public $timestamps=false;

    //pondremos los campos(atributos) que recibiran
    //un valor y cuales no y los almacenaremos es un array

    //tipos de atributos
    protected $fillable =[
    'Nombre',
    'Descripcion',
    'Condicion'
    ];

    protected $guarded = [];

}
