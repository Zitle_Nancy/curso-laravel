<?php

namespace sisventas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoriaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /*autorizamos que el usuario puede hacer el request*/
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //reglas a seguir
        'Nombre'=>'required|max:50',
        'Descripcion'=>'max:256',

        ];
    }
}
