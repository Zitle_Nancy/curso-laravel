<?php

namespace sisventas\Http\Controllers;

use Illuminate\Http\Request;
//porbablememente tenga problemas no recuerdo si era sisventas
use Sisventas\Http\Request;
use Sisventas\Categoria;
use Illuminate\Support\Facades\Redirect;
use Sisventas\Http\Request\CategoriaFormRequest;
use DB;

class CategoriaController extends Controller
{
    //declaramos un constructor

    public function __construct(){

    }

    public function index(Request $request)
    {
    	//recibira como parametros un objeto
    	
    		# obtener los registros de la base de datos
    		if ($request)
    		{
    			//searchText busquedas de categoria
    			$query=trim($request->get('searchText'));
    			//where tiene tres parametros en que atributo, nombre , like que hace la busqueda facil
    			//y los comodines para hacer referencia a la variable query
    			$categorias=DB::table('Categoria')->where('Nombre','LIKE','%'.$query.'%')
    			->where('Condicion','=','1')
    			->orderBy('idCategoria','desc')
    			->paginate(7);
    			return view('almacen.categoria.index',["categorias"=>$categorias,"searchText"=>$query]);
    		}

    }

    public function create(){
    	return view("almacen.categoria.create");

    }

    //para almacenar el objeto categoria de la tabla categoria
    public function store(CategoriaFormRequest $request){
    	$categoria= new Categoria;
    	$categoria->Nombre=$request->get('Nombre');
    	$categoria->Descripcion=$request->get('Descripcion');
    	$categoria->Condicion='1';
    	$categoria->save();
    	return Redirect::to('almacen/categoria');
    }

    public function show($id){
    	return view("almacen.categoria.show",["categoria"=>Categoria::findOrFail($id)]);

    }

    public function edit($id){
    	return view("almacen.categoria.edit",["categoria"=>Categoria::findOrFail($id)]);

    }

    public function update(CategoriaFormRequest $request, $id){
    	$categoria=Categoria::findOrFail($id);
    	$categoria->Nombre=$request->get('Nombre');
    	$categoria->Descripcion=$request->get('Descripcion');
    	$categoria->update();
    	return Redirect::to('almacen/categoria');
    }
    //para destruir un objeto
    public function destroy($id){
    	$categoria=Categoria::findOrFail($id);
    	$categoria->Condicion='0';
    	$categoria->update();
    	return Redirect::to('almacen/categoria');


    }
}
